﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Tabletop_RPG_Utility
{
    /// <summary>
    /// Interaction logic for CharacterCreation.xaml
    /// </summary>
    public partial class CharacterCreation : Window
    {
        int raceStrength = 0;
        int raceDexterity = 0;
        int raceConstitution = 0;
        int raceIntelligence = 0;
        int raceWisdom = 0;
        int raceCharisma = 0;

        int classStrength = 0;
        int classDexterity = 0;
        int classConstitution = 0;
        int classIntelligence = 0;
        int classWisdom = 0;
        int classCharisma = 0;

        int baseCharStrength = 0;
        int baseCharDexterity = 0;
        int baseCharConstitution = 0;
        int baseCharIntelligence = 0;
        int baseCharWisdom = 0;
        int baseCharCharisma = 0;

        int newStrength = 0;
        int newDexterity = 0;
        int newConstitution = 0;
        int newIntelligence = 0;
        int newWisdom = 0;
        int newCharisma = 0;

        string newAlignment;
        string newName;
        string newAge;

        
        /* string charRace;
        string charClass;
        string charAlignment; */


        public CharacterCreation()
        {
            InitializeComponent();
            RaceBox.ItemsSource = LoadRaceComboBoxData();
            ClassBox.ItemsSource = LoadClassComboBoxData();
            AlignmentBox.ItemsSource = LoadAlignmentComboxBoxData();
            /* int baseCharStrength = ConvertStringtoInt(StrengthBox.Text);
            int baseCharDexterity = ConvertStringtoInt(DexterityBox.Text);
            int baseCharConstitution = ConvertStringtoInt(ConstitutionBox.Text);
            int baseCharIntelligence = ConvertStringtoInt(IntelligenceBox.Text);
            int baseCharWisdom = ConvertStringtoInt(WisdomBox.Text);
            int baseCharCharisma = ConvertStringtoInt(CharismaBox.Text); */  //Changing this up to be done a different way and then populate the box with the final value of everything, done in UpdateStatBox() now
            


            newName = "Bill";
            newAge = AgeBox.Text;
            newAlignment = "Neutral";

            UpdateMathLabel();
        }

        private void UpdateStatBoxes() // Recalculates the new stats and displays them in the proper text box
        {
            newStrength = AttrCalculation(baseCharStrength, classStrength, raceStrength);
            newDexterity = AttrCalculation(baseCharDexterity, classDexterity, raceDexterity);
            newConstitution = AttrCalculation(baseCharConstitution, classConstitution, raceConstitution);
            newIntelligence = AttrCalculation(baseCharIntelligence, classIntelligence, raceIntelligence);
            newWisdom = AttrCalculation(baseCharWisdom, classWisdom, raceWisdom);
            newCharisma = AttrCalculation(baseCharCharisma, classCharisma, raceCharisma);

            StrengthBox.Text = newStrength.ToString();
            DexterityBox.Text = newDexterity.ToString();
            ConstitutionBox.Text = newConstitution.ToString();
            IntelligenceBox.Text = newIntelligence.ToString();
            WisdomBox.Text = newWisdom.ToString();
            CharismaBox.Text = newCharisma.ToString();
        }

        static int ConvertStringtoInt(string stringInput)
        {
            string intString = stringInput; // Reads a string then converts from a string to int, hopefully...
            int intOutput = 0;
            if (!Int32.TryParse(intString, out intOutput))
            {
                intOutput = -1;
            }
            return intOutput;
        }

        static public int AttrCalculation(int b, int c, int r)
        {
            return b + c + r;
        }

        private void UpdateMathLabel()
        {
            StrengthMath.Content = String.Format("{0} + {1} + {2}", baseCharStrength, raceStrength, classStrength);
            DexterityMath.Content = String.Format("{0} + {1} + {2}", baseCharDexterity, raceDexterity, classDexterity);
            ConstitutionMath.Content = String.Format("{0} + {1} + {2}", baseCharConstitution, raceConstitution, classConstitution);
            IntelligenceMath.Content = String.Format("{0} + {1} + {2}", baseCharIntelligence, raceIntelligence, classIntelligence);
            WisdomMath.Content = String.Format("{0} + {1} + {2}", baseCharWisdom, raceWisdom, classWisdom);
            CharismaMath.Content = String.Format("{0} + {1} + {2}", baseCharCharisma, raceCharisma, classCharisma);
            UpdateStatBoxes();
        }

        private void CloseWindow_Click(object sender, RoutedEventArgs e)
        {
            MainWindow openMainWindow = new MainWindow();
            openMainWindow.Show();
            this.Close();
        }

        private void Racebox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            string raceValue = RaceBox.SelectedItem.ToString();
            // Reset values to zero before setting the value, in case someone chooses and changes their mind
            raceStrength = 0;
            raceDexterity = 0;
            raceConstitution = 0;
            raceIntelligence = 0;
            raceWisdom = 0;
            raceCharisma = 0;

            if (raceValue == "Hill Dwarf")
            {
                raceConstitution = 2;
                raceWisdom = 1;
                UpdateMathLabel();
            }

            if (raceValue == "Mountain Dwarf")
            {
                raceStrength = 2;
                raceConstitution = 2;
                UpdateMathLabel();
            }

            if (raceValue == "High Elf")
            {
                raceDexterity = 2;
                raceIntelligence = 1;
                UpdateMathLabel();
            }

            if (raceValue == "Wood Elf")
            {
                raceDexterity = 2;
                raceWisdom = 1;
                UpdateMathLabel();
            }

            if (raceValue == "Dark Elf")
            {
                raceDexterity = 2;
                raceCharisma = 1;
                UpdateMathLabel();
            }

            if (raceValue == "Lightfoot Halfling")
            {
                raceDexterity = 2;
                raceCharisma = 1;
                UpdateMathLabel();
            }

            if (raceValue == "Stout Halfling")
            {
                raceDexterity = 2;
                raceConstitution = 1;
                UpdateMathLabel();
            }

            if (raceValue == "Human")
            {
                raceStrength = 1;
                raceDexterity = 1;
                raceConstitution = 1;
                raceIntelligence = 1;
                raceWisdom = 1;
                raceCharisma = 1;
                UpdateMathLabel();
            }

            if (raceValue == "Dragonborn")
            {
                raceStrength = 2;
                raceCharisma = 1;
                UpdateMathLabel();
            }

            if (raceValue == "Forest Gnome")
            {
                raceIntelligence = 2;
                raceDexterity = 1;
                UpdateMathLabel();
            }

            if (raceValue == "Rock Gnome")
            {
                raceIntelligence = 2;
                raceConstitution = 1;
                UpdateMathLabel();
            }

            if (raceValue == "Half-Elf")
            {
                raceCharisma = 2;
                UpdateMathLabel();
            }

            if (raceValue == "Half-Orc")
            {
                raceStrength = 2;
                raceConstitution = 1;
                UpdateMathLabel();
            }

            if (raceValue == "Tiefling")
            {
                raceIntelligence = 1;
                raceCharisma = 2;
                UpdateMathLabel();
            }
        }

        private string[] LoadRaceComboBoxData()
        {
            string[] strArray = {
            "Hill Dwarf",
            "Mountain Dwarf",
            "High Elf",
            "Wood Elf",
            "Dark Elf",
            "Lightfoot Halfling",
            "Stout Halfling",
            "Human",
            "Dragonborn",
            "Forest Gnome",
            "Rock Gnome",
            "Half-Elf",
            "Half-Orc",
            "Tiefling"
        };
            return strArray;
        }

        private string[] LoadClassComboBoxData()
        {
            string[] strArray =
            {
                "Barbarian",
                "Bard",
                "Cleric",
                "Druid",
                "Fighter",
                "Monk",
                "Paladin",
                "Ranger",
                "Rogue",
                "Sorcerer",
                "Warlock",
                "Wizard"
            };
            return strArray;
        }

        private string[] LoadAlignmentComboxBoxData()
        {
            string[] strArray =
            {
                "Lawful Good",
                "Neutral Good",
                "Chaotic Good",
                "Lawful Neutral",
                "Neutral",
                "Chaotic Neutral",
                "Lawful Evil",
                "Neutral Evil",
                "Chaotic Evil"
            };
            return strArray;
        }

        private int[] LoadStandardArray()
        {
            int[] intArray =
            {
                15,
                14,
                13,
                12,
                10,
                8
            };
            return intArray;
        }
        private void NameBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            newName = NameBox.Text;
        }

        private void AlignmentBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            newAlignment = AlignmentBox.SelectedValue.ToString();
            testLabel.Content = newAlignment;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //Creates a new binding using the LoadStandardArray and attaches it to the correct box
            baseStrengthComboBox.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = LoadStandardArray() });
            baseDexterityComboBox.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = LoadStandardArray() });
            baseConstitutionComboBox.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = LoadStandardArray() });
            baseIntelligenceComboBox.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = LoadStandardArray() });
            baseWisdomComboBox.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = LoadStandardArray() });
            baseCharismaComboBox.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = LoadStandardArray() });
        }

        private void StrengthSetBaseStat_Click(object sender, RoutedEventArgs e)
        {
            baseCharStrength = ConvertStringtoInt(baseStrengthComboBox.SelectedValue.ToString());
            UpdateMathLabel();
        }

        private void ClassBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void AgeBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void DexteritySetBaseStat_Click(object sender, RoutedEventArgs e)
        {
            baseCharDexterity = ConvertStringtoInt(baseDexterityComboBox.SelectedValue.ToString());
            UpdateMathLabel();
        }

        private void ConstitutionSetBaseStat_Click(object sender, RoutedEventArgs e)
        {
            baseCharConstitution = ConvertStringtoInt(baseConstitutionComboBox.SelectedValue.ToString());
            UpdateMathLabel();
        }

        private void IntelligenceSetBaseStat_Click(object sender, RoutedEventArgs e)
        {
            baseCharIntelligence = ConvertStringtoInt(baseIntelligenceComboBox.SelectedValue.ToString());
            UpdateMathLabel();
        }

        private void WisdomSetBaseStat_Click(object sender, RoutedEventArgs e)
        {
            baseCharWisdom = ConvertStringtoInt(baseWisdomComboBox.SelectedValue.ToString());
            UpdateMathLabel();
        }

        private void CharismaSetBaseStat_Click(object sender, RoutedEventArgs e)
        {
            baseCharCharisma = ConvertStringtoInt(baseCharismaComboBox.SelectedValue.ToString());
            UpdateMathLabel();
        }

        private void RollBaseStats_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
