﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tabletop_RPG_Utility
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Random rnd = new Random(); // Initialize the random seed and pull what I need off each time in the DiceRoll method
        public MainWindow()
        {
            InitializeComponent();
            
        }

        public int DiceRoll(int sides) // Pass the number of sides you need and get back the next number from Random with a limit on how many sides there are
        {
            try
            {
                return rnd.Next(1, sides);
            }
            catch //simple catch logic to set to 0 if it's an invalid number for now
            {
                return 0;
            }
        }

        private void Press_Click(object sender, RoutedEventArgs e) // Sends the max sides of the text box and displays the results in the testing label
        {
            string intString = howManySide.Text; // Reads from the text box and converts from a string to int
            int requestedSides = 0;
            if (!Int32.TryParse(intString, out requestedSides))
            {
                requestedSides = -1;
            }

            this.testing.Content = DiceRoll(requestedSides);  //Updates the label using the DiceRoll method
        }

        private void NewCharacterButton_Click(object sender, RoutedEventArgs e)
        {
            CharacterCreation newCharacter = new CharacterCreation();
            newCharacter.Show();
            this.Close();
        }
    }
}
