﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tabletop_RPG_Utility
{
    public class Character
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Alignment { get; set; }
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Constitution { get; set; }
        public int Intelligence { get; set; }
        public int Wisdom { get; set; }
        public int Charisma { get; set; }
        public int Experience { get; set; }
        public int Initiative { get; set; }
        public int Speed { get; set; }
        public int ArmorClass { get; set; }
        public int MaxHitPoints { get; set; }
        public int HitDice { get; set; }
        public int Currency { get; set; }
        public int StrengthSavingThrow { get; set; }
        public int DexteritySavingThrow { get; set; }
        public int ConstitutionSavingThrow { get; set; }
        public int IntelligenceSavingThrow { get; set; }
        public int WisdomSavingThrow { get; set; }
        public int CharismaSavingThrow { get; set; }
        public int Height { get; set; }
        public string Eyes { get; set; }
        public string Skin { get; set; }
        public string Hair { get; set; }
        public int Weight { get; set; }

        public Character(string name, int age, string alignment, int strength, int dexterity, int constitution, int intelligence, int wisdom, int charisma)
        {
            Name = name;
            Age = age;
            Alignment = alignment;
            Strength = strength;
            Dexterity = dexterity;
            Constitution = constitution;
            Intelligence = intelligence;
            Wisdom = wisdom;
            Charisma = charisma;
        }
    }
}
